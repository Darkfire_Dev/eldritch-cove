## General

"Eldritch Cove" is a trademark which belongs to Jakub "Darkfire" Kulawik.

## Music

Mournful Skank's songs ("Deeper", "Something is growing") are licensed under CC-BY-NC-SA. Mournful Skank has given Darkfire the permission to use the songs
without SharedAlike, meaning that "Eldritch Cove" is not licensed under CCBYNCSA.
* https://mournful-skank.bandcamp.com/
* https://creativecommons.org/licenses/by-nc-sa/3.0/


Blair Moon's songs ("Few survivors") are licensed under CC-BY-ND.
* https://freemusicarchive.org/music/Blear_Moon
* https://creativecommons.org/licenses/by-nc-nd/4.0/


Darkfire's songs ("Fog calm", "Fog unnerved") and models are licensed under CC-BY-NC-SA.
* https://creativecommons.org/licenses/by-nc-sa/3.0/

## Game assets

"Eldritch Cove" uses several assets (such as, but not limited to: textures, 3D models, sounds) from other games made/published by Frictional Games.

This is under their unwritten agreement to using their assets across free mods of their games:
https://www.frictionalgames.com/forum/thread-17366-post-156672.html#pid156672

All assets used in this mod which don't come from FG have been acquired from online asset stores. They were used under the assumption that they were the author's original work.
* Bucket sound made ingolyrio: https://freesound.org/s/184690/
* Church bell sound by Daniel Simion
* Typewriter font by Christoph Mueller
* Doll monster from Tenebris Lake, retexture and sounds (remixed from TDD sounds) by Darkfire
* Meat drying instructions: http://www.fao.org/docrep/003/x6932e/X6932E02.htm


Other sources include (but are not limited to):
* https://www.turbosquid.com 
* http://www.cadhatch.com/seamless-textures/4588167680
* https://www.textures.com
* https://freesound.org

