# Eldritch Cove - An Amnesia The Dark Descent mod

https://www.moddb.com/mods/eldritch-cove

## Installation

1. Download the mod:
   - **As a zip file**: click the download icon near the top of the page. A file called ```eldritch-cove-master``` will start to download. Once downloaded, extract the archive into your `custom_stories` folder.
   - **Using git**: clone the project into the `custom_stories` folder in your Amnesia install.
2. Rename the ```eldritch-cove-master``` folder to ```Eldritch_Cove```. An incorrect name will break the mod during runtime.

## Known issues
If you encounter a bug not mentioned here, I'd thankful if you reported it to me on ModDb or through Gitlab issues.

1. After loading a savefile, there's models missing, some item callbacks don't work, you can't progress.
   * This is an issue with the way the engine saves the game. If this happens, you will need to load a save from the previous map, progress to the next map and beat it in one go.

2. Julie gets pushed out of the map, or pushed in such a way she can't walk out of the room you meet her in.
   * Reload an earlier save.

3. Julie not showing up in the Tourist Centre (when the player dies and respawns, or any in any other situation).
   * Go outside the Tourist Centre and walk a fair distance, then come back to the Tourist Centre. She should be there.
   * If it doesn't work, you might have not gotten far enough. Go out, turn right and walk at least until bumping into the green car. This distance is enough to respawn her.
   * If the above doesn't fix the issue, please report this, along with your story progression.


## Changelog

Version 1.0 (5th August 2020)
* Initial release of the mod

Version 1.1 (23rd August 2020)
* Launcher and ReadMe updates
* Hopefully fixed the secret door from being too easily accessible 

Version 1.2 (xxx)
* Turned the mod into a Custom Story launchable from the main game for Amnesia's 1.5 update
* Cleaned up and consolidated all the `.txt` files flying around the mod
* Moved development files into a single folder
* Removed the secret language feature; unused maps are now playable from the regular mod menu
* Map cache added to the repo
* Shorten the premenu and improve the splash screens
