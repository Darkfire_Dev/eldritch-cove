<SpawnParticleSystem

	ParticleSystem = "ps_area_fog_dkf.ps"
	
	HeightFromFeet = "0.7"
	HeightAddMin = "0.7"
	HeightAddMax = "3.5"
	
	Density = "0"
		
	Radius = "0"
	
	PSColor = "1 1 1 0.2"
	
	FadePS = "true"
	PSMinFadeEnd = "5"
	PSMinFadeStart = "8"
	PSMaxFadeStart = "17"
	PSMaxFadeEnd = "20"

/>
	
