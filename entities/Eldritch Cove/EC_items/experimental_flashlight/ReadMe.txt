Flashlight for Custom Stories by Darkfire

No Full Conversion required.
Doesn't affect any files from the original Amnesia.

What changes:
- The light of the lantern (small white pointlight at feet and a white spotlight in front of player instead of huge yellow pointlight)
- When it's turned on, no models of player's hand and lantern are visible
- There are entities to replace the lantern and oil with flashlight and batteries
- The light will flicker when player has less then 10% oil

What remains:
- Player can still turn the flashlight on and off with the F button
- Oil drain
- Player won't be able to use the flashlight when they have no oil
- They still have to refill the lantern
- The lantern sounds, unfortunately. They seem impossible to be removed as of now.






Tutorial:

1. Copy the WHOLE contents of

 \experimental_flashlight\Flashlight_Scripts.hps

And paste them to your map.hps.
You can place it wherever in the script, I put it at the far end so that it's out of the way.

2. Place a ScriptArea in your map, ideally somewhere where player will never see it. Let's suppose you named it "YourAreaName".

3. Place the function

SetUpFlashlight("YourAreaName");

in OnEnter. It's important that the function doesn't repeat. If it's placed in OnStart instead, the regular lantern will turn on when player revisits the map.

4. Give the player a lantern. To make it more climatic, use my flashlight item from:

 \experimental_flashlight\Flashlight_item\Flashlight_lantern_dkf.ent

and Tanshaydar's battery item from:

 \experimental_flashlight\item_battery\item_battery.ent

However, a regular lantern and oil will work as well.

5. Make sure the folder "experimental_flashlight" which I supported is included in your Custom Story folder, otherwise the mod will crash when started.








I got the basic idea from Litronom's Cold Breath entity:

https://www.frictionalgames.com/forum/thread-23355.html

