NPC: Standing, can look at player
NPC2: Sitting, can look at player
woman_follow: practically an enemy. Disable triggers and sanity drain.
Follows path nodes, and if currently not on patrol, will randomly play pick-up or yawn animation.

woman_posing - StaticProp only for playing animations. Doesn't respond to scriptfor some reason.

NPC's: scalable. Enemy not.


Model made in Adobe Fuse CC and optimised in Blender.
Animations ported from mixamo.com

Made by Darkfire